package com.examalbo.alboexamenpart1.ui

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.examalbo.alboexamenpart1.R
import com.examalbo.alboexamenpart1.data.model.Expenses
import com.examalbo.alboexamenpart1.data.raw.Raw
import com.examalbo.alboexamenpart1.ui.adapter.AdapterTransaccion
import com.examalbo.alboexamenpart1.ui.viewmodels.ListTransaccionViewModel
import kotlinx.android.synthetic.main.list_transaccion_fragment.*

class ListTransaccion : Fragment() {

    private lateinit var expenses: Expenses
    private lateinit var raw: Raw
    private lateinit var adapterTransaccion: AdapterTransaccion
    private val navigation by lazy { findNavController() }
    companion object {
        fun newInstance() = ListTransaccion()
    }

    private lateinit var viewModel: ListTransaccionViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_transaccion_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ListTransaccionViewModel::class.java)
        init()
    }

    private fun init(){
        listaTransaccion.apply {
            listaTransaccion.layoutManager = LinearLayoutManager(context)
            setHasFixedSize(false)
        }
        raw = Raw()
        expenses = raw.readJson()
        adapterTransaccion = AdapterTransaccion(expenses)
        listaTransaccion.adapter = adapterTransaccion

    }

}