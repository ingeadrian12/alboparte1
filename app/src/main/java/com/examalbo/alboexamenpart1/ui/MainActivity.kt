package com.examalbo.alboexamenpart1.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.Navigation
import com.examalbo.alboexamenpart1.R

class MainActivity : AppCompatActivity() {
    private val mNavigation by lazy { Navigation.findNavController(this,R.id.nav_graph) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}