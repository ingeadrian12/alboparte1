package com.examalbo.alboexamenpart1.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.examalbo.alboexamenpart1.R
import com.examalbo.alboexamenpart1.data.model.Expenses

class AdapterTransaccion(private val expenses: Expenses):RecyclerView.Adapter<AdapterTransaccion.HolderView>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderView {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.content_transaccion, parent, false)
        return HolderView(v)

    }
    init {
        expenses.sortByDescending { it.creation_date }
    }

    override fun onBindViewHolder(holder: HolderView, position: Int) {
       holder.mes.text = convertDate(expenses.get(position).creation_date)
        holder.transacciones_pendientes.text = expenses.get(position).status+""
        holder.transacciones_bloquedas.text = expenses.get(position).status
        holder.ingresos.text = expenses.get(position).operation
        holder.gastos.text = expenses.get(position).operation
        holder.otros.text = expenses.get(position).category
    }

    override fun getItemCount(): Int {
       return expenses.size
    }
    fun convertDate(date:String):String{
        var mes:String?=""
        var mess:String?=""
        mess = date.substring(0,2)
        when(mess){
            "01"->{
                mes = "enero"
            }
            "02"->{
                mes = "febrero"
            }

            "03"->{
                mes = "marzo"
            }

            "04"->{
                mes = "abril"
            }
            "05"->{
                mes = "mayo"
            }
            "06"->{
                mes = "junio"
            }
            "07"->{
                mes = "julio"
            }
            "08"->{
                mes = "agosto"
            }
            "09"->{
                mes = "septiembre"
            }
            "10"->{
                mes = "octubre"
            }

            "11"->{
                mes = "noviembre"
            }

            "12"->{
                mes = "diciembre"
            }

        }
        return mes.toString()

    }

    class HolderView(itemView: View):RecyclerView.ViewHolder(itemView){
        val mes : TextView
        val  transacciones_pendientes : TextView
        val transacciones_bloquedas : TextView
        val ingresos : TextView
        val gastos: TextView
        val  otros : TextView

        init {
            mes = itemView.findViewById(R.id.textView)
            transacciones_pendientes = itemView.findViewById(R.id.textView2)
            transacciones_bloquedas = itemView.findViewById(R.id.textView3)
            ingresos = itemView.findViewById(R.id.textView4)
            gastos = itemView.findViewById(R.id.textView5)
            otros = itemView.findViewById(R.id.textView6)


        }

    }
}