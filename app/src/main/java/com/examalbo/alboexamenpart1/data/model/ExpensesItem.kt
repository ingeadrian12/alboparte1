package com.examalbo.alboexamenpart1.data.model

data class ExpensesItem(
    var amount: Double,
    var category: String,
    var creation_date: String,
    var description: String,
    var operation: String,
    var status: String,
    var uuid: Int
)