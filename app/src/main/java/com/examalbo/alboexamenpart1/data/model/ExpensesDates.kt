package com.examalbo.alboexamenpart1.data.model

data class ExpensesDates(var mes:String,var pending:Int, var done:Int,var blocked:Int) {
    constructor() : this("",0,0,0)
}