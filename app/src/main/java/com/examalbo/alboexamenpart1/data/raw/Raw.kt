package com.examalbo.alboexamenpart1.data.raw

import com.examalbo.alboexamenpart1.common.ReadRaw
import com.examalbo.alboexamenpart1.data.model.Expenses
import com.examalbo.alboexamenpart1.data.model.ExpensesDates
import com.examalbo.alboexamenpart1.data.model.ExpensesItem

class Raw {
    private  val readRaw: ReadRaw
    private var expensesDate: List<Expenses>
    private var expensesDates :ExpensesDates
    private var pendientes:Int = 0
    private  var bloquedas: Int =0
    private var ingresos: Int =0
    private var gastos:Int =0
    private var  otros : Int =0
    private var boolean: String = "diciembre"

    fun readJson():Expenses{
        getExpensesDate(readRaw.read())
       return readRaw.read()

    }

    fun convertDate(date:String):String{
        var mes:String?=""
        var mess:String?=""
        mess = date.substring(0,2)
        when(mess){
            "01"->{
                mes = "enero"
            }
            "02"->{
                mes = "febrero"
            }

            "03"->{
                mes = "marzo"
            }

            "04"->{
                mes = "abril"
            }
            "05"->{
                mes = "mayo"
            }
            "06"->{
                mes = "junio"
            }
            "07"->{
                mes = "julio"
            }
            "08"->{
                mes = "agosto"
            }
            "09"->{
                mes = "septiembre"
            }
            "10"->{
                mes = "octubre"
            }

            "11"->{
                mes = "noviembre"
            }

            "12"->{
                mes = "diciembre"
            }

        }
        return mes.toString()

    }

    init {
        readRaw = ReadRaw()
        expensesDate = listOf(Expenses())
        expensesDates = ExpensesDates()

    }

    fun getExpensesDate(expenses: Expenses):List<Expenses>{
        var expensesDates:ArrayList<ExpensesDates>
        var valida:ExpensesDates
        valida = ExpensesDates()
        expensesDates = ArrayList(12)
        expenses.sortByDescending {it.creation_date  }
        for ((index,value) in expenses.withIndex()){
            if (convertDate(expenses.get(index).creation_date).equals("diciembre")){

                if (boolean.equals("diciembre")){
                    pendientes =0
                    gastos = 0
                    bloquedas =0
                    boolean = "noviembre"
                    valida.mes = "diciembre"
                    valida.blocked =0
                    valida.done =0
                    valida.pending =0
                }


                if (value.status.equals("pending")){
                  valida.pending =pendientes++

                }
                if (value.status.equals("done")){
                    valida.done = gastos++

                }
                if (value.status.equals("rejected")){
                   valida.blocked = bloquedas++
                }

            }

            if (convertDate(expenses.get(index).creation_date).equals("noviembre")){

                if (boolean.equals("noviembre")){

                    expensesDates.add(0,valida)
                    valida.blocked =0
                    valida.done =0
                    valida.pending =0
                    pendientes =0
                    gastos = 0
                    bloquedas =0
                    boolean = "octubre"
                    valida.mes = "noviembre"
                }


                if (value.status.equals("pending")){
                    valida.pending =pendientes++

                }
                if (value.status.equals("done")){
                    valida.done = gastos++

                }
                if (value.status.equals("rejected")){
                    valida.blocked = bloquedas++
                }

            }

            if (convertDate(expenses.get(index).creation_date).equals("octubre")){

                if (boolean.equals("octubre")){

                    expensesDates.add(1,valida)
                    valida.blocked =0
                    valida.done =0
                    valida.pending =0
                    pendientes =0
                    gastos = 0
                    bloquedas =0
                    boolean = "septiembre"
                    valida.mes = "octubre"
                }

                if (value.status.equals("pending")){
                    valida.pending =pendientes++

                }
                if (value.status.equals("done")){
                    valida.done = gastos++

                }
                if (value.status.equals("rejected")){
                    valida.blocked = bloquedas++
                }

            }
            if (convertDate(expenses.get(index).creation_date).equals("septiembre")){

                if (boolean.equals("septiembre")){
                    expensesDates.add(2,valida)
                    valida.blocked =0
                    valida.done =0
                    valida.pending =0
                    pendientes =0
                    gastos = 0
                    bloquedas =0
                    boolean = "agosto"
                    valida.mes = "septiembre"
                }

                if (value.status.equals("pending")){
                    valida.pending =pendientes++

                }
                if (value.status.equals("done")){
                    valida.done = gastos++

                }
                if (value.status.equals("rejected")){
                    valida.blocked = bloquedas++
                }
            }

            if (convertDate(expenses.get(index).creation_date).equals("agosto")){
                if (boolean.equals("agosto")){
                    expensesDates.add(3,valida)
                    pendientes =0
                    gastos = 0
                    bloquedas =0
                    boolean = "julio"
                }
                valida.mes = "agosto"
                if (value.status.equals("pending")){
                    valida.pending =pendientes++

                }
                if (value.status.equals("done")){
                    valida.done = gastos++

                }
                if (value.status.equals("rejected")){
                    valida.blocked = bloquedas++
                }


            }

            if (convertDate(expenses.get(index).creation_date).equals("julio")){

                if (boolean.equals("julio")){
                    expensesDates.add(4,valida)
                    pendientes =0
                    gastos = 0
                    bloquedas =0
                    boolean = "junio"
                }
                valida.mes = "junio"
                if (value.status.equals("pending")){
                    valida.pending =pendientes++

                }
                if (value.status.equals("done")){
                    valida.done = gastos++

                }
                if (value.status.equals("rejected")){
                    valida.blocked = bloquedas++
                }


            }
        }

        return  expensesDate
    }


    fun intiValues(){


    }

}